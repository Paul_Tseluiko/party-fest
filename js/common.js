$(document).ready(function () {

    if ($(window).width() > '1025') {
        $('.artist_card__parent-gallery').removeClass('js-sl-gallery').removeClass('owl-carousel').removeClass('owl-theme').removeClass('owl-loaded').removeClass('owl-drag');
    }


    $('.js-sl-gallery').owlCarousel({
        items: 2,
        margin:10,
        responsive: {
            0: {
                items: 2,
            },
            480: {
                items: 3,
            },
            768: {
                items: 4,
            }
        }
    });

    $('.js-index-mobile-slider').owlCarousel({
        items: 1,
        dots: false,
        nav: true
    });

    $(".b-hamburger").click(function (event) {
        event.preventDefault();
        $("span").toggleClass("active");
        $(".mobile-menu").toggleClass("toggle-menu");
    });

    $('.mobile-menu').on('click', 'a', function () {
        $(".mobile-menu").removeClass("toggle-menu");
        $("span").removeClass("active");
    });

    // lazyload
    $('img, .js-bg').unveil({
        offset: 200,
        throttle: 200
    });

    $('select').selectric();

    $("[data-fancybox]").fancybox({
        // Options will go here
    });
    
    var blocksArtists = $('.specialists__item');
    var elems = document.querySelectorAll('.specialists__item');
    // elems.splice(4);
    console.log(elems);
    elems.forEach(function(el, i){
      elems[i].className += ' elems';
      console.log([i]);
    });
    // console.log(blocksArtists);
    $(function () {
        objectFitImages()
    });

    $(window).resize(function () {
        if ($(window).width() <= '767') {
            $('.to-clone').clone().appendTo('.holidays-mobile__numb');
            $('.to-clone').nextAll('.to-clone').remove();
        }
    });

});
